//
// Created by notnaturalselection on 18.09.2021.
//

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include "pa2345.h"
#include "banking.h"

#define BUF_SIZE 150

void log_start(int log_file_fd, int p_index, balance_t start_balance, timestamp_t at) {
    char buf[BUF_SIZE] = {0};
    sprintf(buf, log_started_fmt, at, p_index, getpid(), getppid(), start_balance);
    write(log_file_fd, buf, strlen(buf));
    write(STDOUT_FILENO, buf, strlen(buf));
}

void log_all_started(int log_file_fd, int p_index, timestamp_t at) {
    char buf[BUF_SIZE] = {0};
    sprintf(buf, log_received_all_started_fmt, at, p_index);
    write(log_file_fd, buf, strlen(buf));
    write(STDOUT_FILENO, buf, strlen(buf));
}

void log_done(int log_file_fd, int p_index, balance_t end_balance, timestamp_t at) {
    char buf[BUF_SIZE] = {0};
    sprintf(buf, log_done_fmt, at, p_index, end_balance);
    write(log_file_fd, buf, strlen(buf));
    write(STDOUT_FILENO, buf, strlen(buf));
}

void log_all_done(int log_file_fd, int p_index, timestamp_t at) {
    char buf[BUF_SIZE] = {0};
    sprintf(buf, log_received_all_done_fmt, at, p_index);
    write(log_file_fd, buf, strlen(buf));
    write(STDOUT_FILENO, buf, strlen(buf));
}

void log_transfer_out(int log_file_fd, local_id p_id, local_id to, balance_t amount, timestamp_t at) {
    char buf[BUF_SIZE] = {0};
    sprintf(buf, log_transfer_out_fmt, at, p_id, amount, to);
    write(log_file_fd, buf, strlen(buf));
    write(STDOUT_FILENO, buf, strlen(buf));
}

void log_transfer_in(int log_file_fd, local_id p_id, local_id from, balance_t amount, timestamp_t at) {
    char buf[BUF_SIZE] = {0};
    sprintf(buf, log_transfer_in_fmt, at, p_id, amount, from);
    write(log_file_fd, buf, strlen(buf));
    write(STDOUT_FILENO, buf, strlen(buf));
}
