//
// Created by notnaturalselection on 18.09.2021.
//

#ifndef PA1_MSG_H
#define PA1_MSG_H

#include <malloc.h>
#include <string.h>
#include "ipc.h"
#include "banking.h"

Message *alloc_msg(MessageType type);

Message *create_msg(MessageType type, void *buf, uint16_t payload_len, _Bool set_lamport_time);

Message *create_empty_msg(MessageType type);

Message *create_msg_started(int8_t p_index, balance_t balance);

Message *create_msg_done(int8_t p_index, balance_t balance);

#endif //PA1_MSG_H
