//
// Created by notnaturalselection on 18.09.2021.
//

#ifndef PA1_PIPES_H
#define PA1_PIPES_H

#include <stdint.h>

typedef struct {
    int pipe_rw_descriptors[2];
} simplex_pipe;

typedef struct {
    int8_t p_count;
    int8_t local_id;
    simplex_pipe ** pipes;
} pipe_array;

pipe_array *init_pipes(long p_count);

void close_unused_descriptors(int p_index, pipe_array *pipes);

#endif //PA1_PIPES_H
