//
// Created by notnaturalselection on 15.12.2021.
//

#include "lamport_time.h"


static timestamp_t total;

timestamp_t get_lamport_time() {
    return ++total;
}

void set_if_greater(timestamp_t other) {
    total = other > total ? other : total;
}
