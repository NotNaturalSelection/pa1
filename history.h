//
// Created by notnaturalselection on 15.12.2021.
//

#ifndef PA1_HISTORY_H
#define PA1_HISTORY_H

#include "banking.h"

BalanceHistory *init_balance_history(local_id p_id, balance_t start_balance);

void handle_transfer(BalanceHistory *ptr, TransferOrder *transfer, local_id p_id, timestamp_t at, timestamp_t message_time);
#endif //PA1_HISTORY_H
