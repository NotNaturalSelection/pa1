//
// Created by notnaturalselection on 06.11.2021.
//

#include "ipc.h"
#include "banking.h"
#include "msg.h"

void transfer(void *parent_data, local_id src, local_id dst, balance_t amount) {
    TransferOrder transfer;
    transfer.s_amount = amount;
    transfer.s_dst = dst;
    transfer.s_src = src;
    send(parent_data, src, create_msg(TRANSFER, &transfer, sizeof(TransferOrder)));
    Message received;
    receive(parent_data, dst, &received);
}
