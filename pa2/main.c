#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <fcntl.h>
#include "pipes.h"
#include "common.h"
#include "log.h"
#include "ipc.h"
#include "msg.h"
#include "banking.h"
#include "history.h"

void log_pipes(pipe_array *pipes) {
    int log_file_fd = open(pipes_log, O_RDWR | O_CREAT | O_APPEND);
    char buf[50000] = {0};
    for (int8_t i = 0; i < pipes->p_count; ++i) {
        for (int8_t j = 0; j < pipes->p_count; ++j) {
            char minibuf[400] = {0};
            if (i != j) {
                sprintf(minibuf, "Pipe from process %d to %d has read_fd %d and write_fd %d\n", i, j,
                        pipes->pipes[i][j].pipe_rw_descriptors[0], pipes->pipes[i][j].pipe_rw_descriptors[1]);
                strcat(buf, minibuf);
            }
        }
    }
    write(log_file_fd, buf, strlen(buf));
    close(log_file_fd);
}

int receive_all(MessageType type, pipe_array *pipes, Message *collector) {
    int count = 0;
    for (local_id i = 1; i < pipes->p_count; ++i) {

        Message msg;
        if (pipes->local_id != i) {
            if (receive(pipes, i, &msg) != 0 || msg.s_header.s_type != type) {
                count = -1;
            } else if (collector != NULL) {
                Message *copy = alloc_msg(type);
                memcpy(copy, &msg, sizeof(Message));
                collector[count++] = *copy;
            }
        }
    }
    return count;
}

void process_work(local_id p_index, pipe_array *pipes, int log_file_fd, balance_t start_balance) {
    BalanceHistory *history = init_balance_history(p_index, start_balance);

    pipes->local_id = p_index;
    close_unused_descriptors(p_index, pipes);
    log_start(log_file_fd, p_index, start_balance);//log start
    Message *started = create_msg_started(p_index, start_balance);
    send_multicast(pipes, started);//write to pipes about start

    receive_all(STARTED, pipes, NULL);//read from pipes about start
    log_all_started(log_file_fd, p_index);//log received all started

    Message msg;
    while (msg.s_header.s_type != STOP) {
        int received = -1;
        while (received == -1) {
            received = receive_any(pipes, &msg);
        }
        if (msg.s_header.s_type == TRANSFER) {
            TransferOrder *transfer = (TransferOrder *) msg.s_payload;
            handle_transfer(history, /*msg.s_header.s_local_time,*/ transfer, p_index);
            if (transfer->s_src == p_index) {
                send(pipes, transfer->s_dst, &msg);
                log_transfer_out(log_file_fd, p_index, transfer->s_dst, transfer->s_amount);
            } else {
                send(pipes, PARENT_ID, create_empty_msg(ACK));
                log_transfer_in(log_file_fd, p_index, transfer->s_src, transfer->s_amount);
            }
        }
    }

    log_done(log_file_fd, p_index, history->s_history[history->s_history_len - 1].s_balance);
    Message *done = create_msg_done(p_index, history->s_history[history->s_history_len].s_balance);
    send_multicast(pipes, done);//write to pipes about done
    receive_all(DONE, pipes, NULL);//read from pipes about done
    log_all_done(log_file_fd, p_index);//log received all done

    send(pipes, PARENT_ID, create_msg(BALANCE_HISTORY, history, 2 + sizeof(BalanceState) * history->s_history_len));

    free(started);
    free(done);
    free(history);
}

void collectBalanceHistories(long workers_number, pipe_array *pipes) {
    Message balance_histories[workers_number];
    AllHistory all_history;
    all_history.s_history_len = 0;
    receive_all(BALANCE_HISTORY, pipes, balance_histories);
    uint8_t max = 0;
    for (int i = 0; i < workers_number; ++i) {
        memcpy(&all_history.s_history[all_history.s_history_len++], &balance_histories[i].s_payload,
               sizeof(BalanceHistory));
        if (all_history.s_history[all_history.s_history_len-1].s_history_len > max) {
            max = all_history.s_history[all_history.s_history_len-1].s_history_len;
        }
    }
    for (int i = 0; i < workers_number; ++i) {
        if (all_history.s_history[i].s_history_len < max) {
            for (int j = all_history.s_history[i].s_history_len; j < max; ++j) {
                all_history.s_history[i].s_history[j].s_time = j;
                all_history.s_history[i].s_history[j].s_balance = all_history.s_history[i].s_history[j-1].s_balance;
                all_history.s_history[i].s_history_len++;
            }
        }
    }
    print_history(&all_history);
}

int main(int argc, char **argv) {
    int events_log_file_fd = open(events_log, O_RDWR | O_APPEND | O_CREAT);

    if (argc < 3) {
        printf("No arguments provided\n");
        return EXIT_FAILURE;
    }

    long workers_number = strtol(argv[2], NULL, 10);
    if (strcmp(argv[1], "-p") != 0 || workers_number < 0 || workers_number > MAX_PROCESS_ID) {
        printf("Illegal arguments\n");
        return -1;
    }
    balance_t balances[workers_number];

    for (int i = 0; i < workers_number; ++i) {
        balances[i] = strtol(argv[3 + i], NULL, 10);
    }


    pipe_array *pipes = init_pipes(workers_number + 1);
    log_pipes(pipes);

    __pid_t fork_pids[workers_number];
    __pid_t pid;
    for (local_id i = 1; i < workers_number + 1; ++i) {
        switch (pid = fork()) {
            case -1:
                //should not happen
                printf("Error occurred. Errno: %d", errno);
                exit(EXIT_FAILURE);
            case 0:
                process_work(i, pipes, events_log_file_fd, balances[i - 1]);
                exit(EXIT_SUCCESS);
            default:
                fork_pids[i] = pid;
        }
    }
    close_unused_descriptors(PARENT_ID, pipes);
    receive_all(STARTED, pipes, NULL);
    log_all_started(events_log_file_fd, PARENT_ID);
    //useful job
    bank_robbery(pipes, workers_number);
    send_multicast(pipes, create_empty_msg(STOP));
    //end of useful job
    receive_all(DONE, pipes, NULL);
    log_all_done(events_log_file_fd, PARENT_ID);
    collectBalanceHistories(workers_number, pipes);


    for (int i = 0; i < workers_number; ++i) {
        waitpid(fork_pids[i], NULL, 0);
    }
    close(events_log_file_fd);

    return EXIT_SUCCESS;
}
