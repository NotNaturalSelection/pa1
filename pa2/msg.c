//
// Created by notnaturalselection on 18.09.2021.
//

#include <unistd.h>
#include "msg.h"
#include "pa2345.h"
#include "banking.h"
#define BUF_SIZE 150

Message *alloc_msg(MessageType type) {
    Message *msg = calloc(1, sizeof(Message));
    msg->s_header.s_magic = MESSAGE_MAGIC;
    msg->s_header.s_type = type;
    return msg;
}

Message *create_msg(MessageType type, void *buf, uint16_t payload_len) {
    Message *msg = alloc_msg(type);
    msg->s_header.s_payload_len = payload_len;
    msg->s_header.s_local_time = get_physical_time();
    if (msg->s_header.s_payload_len > 0 && buf != NULL) {
        memcpy(msg->s_payload, buf, msg->s_header.s_payload_len);
    }
    return msg;
}

Message *create_empty_msg(MessageType type) {
    return create_msg(type, NULL, 0);
}

Message *create_msg_started(local_id p_index, balance_t balance) {
    char buf[BUF_SIZE] = {0};
    sprintf(buf, log_started_fmt, get_physical_time(), p_index, getpid(), getppid(), balance);
    return create_msg(STARTED, buf, strlen(buf));
}

Message *create_msg_done(local_id p_index, balance_t balance) {
    char buf[BUF_SIZE] = {0};
    sprintf(buf, log_done_fmt, get_physical_time(), p_index, balance);
    return create_msg(DONE, buf, strlen(buf));
}
