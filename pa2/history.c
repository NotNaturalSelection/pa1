//
// Created by notnaturalselection on 15.12.2021.
//

#include <malloc.h>
#include "history.h"

BalanceHistory *init_balance_history(local_id p_id, balance_t start_balance) {
    BalanceHistory *history = calloc(1, sizeof(BalanceHistory));
    history->s_id = p_id;
    history->s_history[0].s_balance = start_balance;
    history->s_history_len = 1;
    return history;
}

void handle_transfer(BalanceHistory *ptr,/* timestamp_t at,*/ TransferOrder *transfer, local_id p_id) {
    timestamp_t at = get_physical_time();
    balance_t prev_balance = ptr->s_history[ptr->s_history_len - 1].s_balance;
    for (timestamp_t i = ptr->s_history_len; i < at; ++i) {
        ptr->s_history[i].s_balance = prev_balance;
        ptr->s_history[i].s_time = i;
    }
    ptr->s_history_len = at - 1;
    BalanceState *balancePoint = &ptr->s_history[at];
    if (p_id == transfer->s_src) {
        balancePoint->s_balance = prev_balance - transfer->s_amount;
    } else {
        balancePoint->s_balance = prev_balance + transfer->s_amount;
    }
    ptr->s_history_len = at+1;
    balancePoint->s_time = at;
}
