//
// Created by notnaturalselection on 18.09.2021.
//

#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include "ipc.h"
#include "pipes.h"


int send(void *self, local_id dst, const Message *msg) {
    pipe_array *arr = (pipe_array *) self;
    long result = write(arr->pipes[arr->local_id][dst].pipe_rw_descriptors[1], msg, msg->s_header.s_payload_len + 8);
    if (result == 0) {
        return 0;
    } else {
        return -1;
    }
}

int send_multicast(void *self, const Message *msg) {
    pipe_array *arr = (pipe_array *) self;
    int overall_status = 0;
//    printf("about to send inside pid %d\n",arr->local_id);
    for (local_id i = 0; i < arr->p_count; ++i) {
        if (i != arr->local_id) {
//            printf("send: pid %d, to %d\n", arr->local_id, i);
            if (send(arr, i, msg) != 0) {
                overall_status = -1;
            }
        }
    }
    return overall_status;
}

int receive(void *self, local_id from, Message *msg) {
    pipe_array *arr = (pipe_array *) self;
    long result = -1;
    while (result == -1) {
//        printf("trying receive: pid %d, from %d, errno %d\n", arr->local_id, from, errno);
        result = read(arr->pipes[from][arr->local_id].pipe_rw_descriptors[0], msg, 8);
    }
//    printf("received: pid %d, from %d\n", arr->local_id, from);
//    result = read(arr->pipes[from][arr->local_id].pipe_rw_descriptors[0], msg, 8);
//    if (result == 0) {
        if (msg->s_header.s_payload_len > 0) {
            read(arr->pipes[from][arr->local_id].pipe_rw_descriptors[0], msg->s_payload, msg->s_header.s_payload_len);
        }
        return 0;
//    }
    return -1;


//    pipe_array *arr = (pipe_array *) self;
//    read(arr->pipes[from][arr->local_id].pipe_rw_descriptors[0], msg, 8);
//    printf("received from %d by %d\n", from, arr->local_id);
//    if (msg->s_header.s_payload_len > 0) {
//        read(arr->pipes[from][arr->local_id].pipe_rw_descriptors[0], msg->s_payload, msg->s_header.s_payload_len);
//    }
//    return 0;

}

int receive_any(void *self, Message *msg) {
    pipe_array *arr = (pipe_array *) self;
    for (int i = 0; i < arr->p_count; ++i) {
        if (i != arr->local_id) {
            long result = read(arr->pipes[i][arr->local_id].pipe_rw_descriptors[0], msg, 8);
            if (result >= 0) {
                read(arr->pipes[i][arr->local_id].pipe_rw_descriptors[0], msg->s_payload, msg->s_header.s_payload_len);
                return 0;
            }
        }
    }
    return -1;
}
