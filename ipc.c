//
// Created by notnaturalselection on 18.09.2021.
//

#include <unistd.h>
#include "ipc.h"
#include "pipes.h"
#include "banking.h"
#include "lamport_time.h"


int send(void *self, local_id dst, const Message *msg) {
    pipe_array *arr = (pipe_array *) self;
    long result = write(arr->pipes[arr->local_id][dst].pipe_rw_descriptors[1], msg, msg->s_header.s_payload_len + 8);
    if (result == 0) {
        return 0;
    } else {
        return -1;
    }
}

int send_multicast(void *self, const Message *msg) {
    pipe_array *arr = (pipe_array *) self;
    int overall_status = 0;
    for (local_id i = 0; i < arr->p_count; ++i) {
        if (i != arr->local_id) {
            if (send(arr, i, msg) != 0) {
                overall_status = -1;
            }
        }
    }
    return overall_status;
}

int receive(void *self, local_id from, Message *msg) {
    pipe_array *arr = (pipe_array *) self;
    long result = -1;
    while (result == -1) {
        result = read(arr->pipes[from][arr->local_id].pipe_rw_descriptors[0], msg, 8);
    }
    if (msg->s_header.s_payload_len > 0) {
        read(arr->pipes[from][arr->local_id].pipe_rw_descriptors[0], msg->s_payload, msg->s_header.s_payload_len);
    }
    return 0;
}

int receive_any(void *self, Message *msg) {
    pipe_array *arr = (pipe_array *) self;
    for (int i = 0; i < arr->p_count; ++i) {
        if (i != arr->local_id) {
            long result = read(arr->pipes[i][arr->local_id].pipe_rw_descriptors[0], msg, 8);
            if (result >= 0) {
                read(arr->pipes[i][arr->local_id].pipe_rw_descriptors[0], msg->s_payload, msg->s_header.s_payload_len);
//                set_if_greater(msg->s_header.s_local_time);
//                msg->s_header.s_local_time = get_lamport_time();
                return 0;
            }
        }
    }
    return -1;
}
