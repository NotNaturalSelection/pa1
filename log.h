//
// Created by notnaturalselection on 18.09.2021.
//

#ifndef PA1_LOG_H
#define PA1_LOG_H

#include <stdio.h>
#include "msg.h"

void log_start(int log_file_fd, int p_index, balance_t start_balance, timestamp_t at);

void log_all_started(int log_file_fd, int p_index, timestamp_t at);

void log_done(int log_file_fd, int p_index, balance_t end_balance, timestamp_t at);

void log_transfer_out(int log_file_fd, local_id p_id, local_id to, balance_t amount, timestamp_t at);

void log_transfer_in(int log_file_fd, local_id p_id, local_id from, balance_t amount, timestamp_t at);

void log_all_done(int log_file_fd, int p_index, timestamp_t at);

#endif //PA1_LOG_H
