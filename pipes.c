//
// Created by notnaturalselection on 18.09.2021.
//

#include <fcntl.h>
#include "pipes.h"
#include "malloc.h"
#include "unistd.h"

//// pipes are simplex so pipe[i][j] allows to write from process i to j and pipe[j][i] allows to write backwards
//// For 3 workers and parent process
////      p   1   2   3  i
////  * ----------------->
//// p | [X] [ ] [ ] [ ]
//// 1 | [ ] [X] [ ] [ ]
//// 2 | [ ] [ ] [X] [ ]
//// 3 | [ ] [ ] [ ] [X]
//// j v
pipe_array *init_pipes(long p_count) {
    pipe_array *result = calloc(1, sizeof(pipe_array));
    result->p_count = p_count;
    simplex_pipe **arr = calloc(p_count, sizeof(void *));
    for (int i = 0; i < p_count; ++i) {
        arr[i] = calloc(p_count, sizeof(simplex_pipe));
    }

    for (int i = 0; i < p_count; ++i) {
        for (int j = 0; j < p_count; ++j) {
            if (i != j) {
                pipe(arr[i][j].pipe_rw_descriptors);
                fcntl(arr[i][j].pipe_rw_descriptors[0], F_SETFL,
                      fcntl(arr[i][j].pipe_rw_descriptors[0], F_GETFL) | O_NONBLOCK);
                fcntl(arr[i][j].pipe_rw_descriptors[1], F_SETFL,
                      fcntl(arr[i][j].pipe_rw_descriptors[1], F_GETFL) | O_NONBLOCK);
            }
        }
    }
    result->pipes = arr;
    return result;
}

//// When new process starts it copies every open descriptor of forked process. So we need to close unused
//// For process 2 we will close pipes marked with 'c'.
//// In every used pipe we will close its 'read' or 'write' descriptor depending on the pipe usage
////      p   1   2   3  i
////  * ----------------->
//// p | [X] [C] [r] [C]
//// 1 | [C] [X] [r] [C]
//// 2 | [w] [w] [X] [w]
//// 3 | [C] [C] [r] [X]
//// j v
void close_unused_descriptors(int p_index, pipe_array *pipes) {
    for (int i = 0; i < pipes->p_count; ++i) {
        for (int j = 0; j < pipes->p_count; ++j) {
            if (i != j && i != p_index && j != p_index) {
                close(pipes->pipes[i][j].pipe_rw_descriptors[0]);
                close(pipes->pipes[i][j].pipe_rw_descriptors[1]);
            } else if (i == p_index) {
                close(pipes->pipes[i][j].pipe_rw_descriptors[0]);
            } else if (j == p_index) {
                close(pipes->pipes[i][j].pipe_rw_descriptors[1]);
            }
        }
    }
}
